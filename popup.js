const playButton = document.getElementById('play-button')
const stopButton = document.getElementById('stop-button')
const textInput = document.getElementById('text')
const speedInput = document.getElementById('speed')
const zoom = document.getElementById('zoom')

let currentCharacter

zoom.addEventListener('click', () => {
    zoomText(textInput)
})

playButton.addEventListener('click', () => {
  playText(textInput.value)
})

stopButton.addEventListener('click', stopText)

speedInput.addEventListener('input', () => {
  stopText()
  playText(utterance.text.substring(currentCharacter))
})

const utterance = new SpeechSynthesisUtterance()

utterance.addEventListener('end', () => {
  textInput.disabled = false
})

utterance.addEventListener('boundary', e => {
  currentCharacter = e.charIndex
})

function playText(text) {
  if (speechSynthesis.speaking) {
    return speechSynthesis.resume()
  }

  if (speechSynthesis.speaking) return

  utterance.text = text
  utterance.rate = speedInput.value || 1
  textInput.disabled = true
  speechSynthesis.speak(utterance)
}

function stopText() {
  speechSynthesis.resume()
  speechSynthesis.cancel()
}

function zoomText(textInput){
    var style = window.getComputedStyle(textInput, null).getPropertyValue('font-size');
    var currentSize = parseInt(style);
    
    if(currentSize < 40) currentSize = currentSize + 2
    console.log(currentSize)
    textInput.style.fontSize = currentSize + 'px'
}